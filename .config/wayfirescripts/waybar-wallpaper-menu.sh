#!/bin/bash


SELECTED=`echo -e "Select Random Wallpaper
Choose Wallpaper" | rofi -dmenu`

[ -z "${SELECTED}" ] && exit

case "${SELECTED}" in

    "Select Random Wallpaper")
	sed -i 's/WALLPAPER=.*/WALLPAPER="RANDOM"/' ~/.config/wayfirescripts/wayfirewallpaperstate.sh
	;;

    "Choose Wallpaper")
	SELECTED=`ls ~/.config/wallpapers// | rofi -dmenu`

	[ -z "${SELECTED}" ] && exit
	
	sed -i 's/WALLPAPER=.*/WALLPAPER="'"$SELECTED"'"/' ~/.config/wayfirescripts/wayfirewallpaperstate.sh
	;;

esac

~/.config/wayfirescripts/wayfirethemerefresh.sh 
