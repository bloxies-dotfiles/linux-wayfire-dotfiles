#!/bin/sh

. ${HOME}/.config/wayfirescripts/wayfirewallpaperstate.sh 

if [ "${WALLPAPER}" == "RANDOM" ]
then
    WALLPAPER=`find ${HOME}/.config/wallpapers/ -type f | shuf -n 1 | cut -d '/' -f 6-7`
fi

# Set current wallpaper as file so we can access from swaylock etc
cp "${HOME}/.config/wallpapers/${WALLPAPER}" ${HOME}/.config/currentwallpaper.png 

# Bugged and does not work because swaybg dies when waybar is killed, therefore had to make external daemon outside of waybar script to set wallpaper properly
# swaybg -i "${HOME}/.config/wallpapers/${WALLPAPER}" &

case "${COLOURSCHEME}" in

    "WALDARK")
	# Reset colourschemes and add wallpaper
	# wpg -R "${HOME}/.config/wallpapers/${WALLPAPER}"
	wpg -a "${HOME}/.config/wallpapers/${WALLPAPER}"
	
	wpg -s "${HOME}/.config/wallpapers/${WALLPAPER}"

	pywal-discord -p $HOME/.config/Lightcord_BD/themes/
	;;
    
    "WALLIGHT")
	# Reset colourschemes and add wallpaper
	# wpg -R "${HOME}/.config/wallpapers/${WALLPAPER}"
	wpg -a "${HOME}/.config/wallpapers/${WALLPAPER}"

	wpg -Ls "${HOME}/.config/wallpapers/${WALLPAPER}"

	pywal-discord -p $HOME/.config/Lightcord_BD/themes/
	;;
    
    *)
	# Reset colourschemes and add wallpaper
	# wpg -R "${HOME}/.config/wallpapers/${WALLPAPER}"
	wpg -a "${HOME}/.config/wallpapers/${WALLPAPER}"

	# wpg -Ti "${HOME}/.config/wallpapers/${WALLPAPER}" ${COLOURSCHEME} && pywal-discord -p $HOME/.config/Lightcord_BD/themes/

	wpg --theme ${COLOURSCHEME}

	pywal-discord -p $HOME/.config/Lightcord_BD/themes/


	# Make sure to reload new theme
	# wpg -s "${HOME}/.config/wallpapers/${WALLPAPER}"
	;;
    
esac

# Signal to the wallpaper/refresh daemon - Kinda jank ngl
tee ${HOME}/.config/wayfirescripts/wayfirewallpaperdaemon.update<<EOF
${RANDOM}
EOF

