#!/bin/bash

ACTIVENETWORK=`nmcli -t -f name connection show --active`

SELECTED=`echo -e "Appearance
 Set Wallpaper
 Set Theme
Change Brightness
Connect to the Internet (Currently connected to ${ACTIVENETWORK})" | rofi -dmenu`

[ -z "${SELECTED}" ] && exit

case "${SELECTED}" in

    " Set Wallpaper")
	. ~/.config/wayfirescripts/waybar-wallpaper-menu.sh 
	;;

    " Set Theme")
	. ~/.config/wayfirescripts/waybar-theme-menu.sh 
	;;

    "Change Brightness")
	echo "Italian"
	;;

    "Connect to the Internet (Currently connected to ${ACTIVENETWORK})")
	echo "Internet"
	;;
    
esac
