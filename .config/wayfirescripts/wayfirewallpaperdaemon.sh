#!/bin/sh

# Start waybar if needed
[ -z `pidof waybar` ] && waybar &

# Wait till signal is received, and then set wallpaper
while true; do
    inotifywait -e create,modify ~/.config/wayfirescripts/wayfirewallpaperdaemon.update
    killall swaybg
    swaybg -i ~/.config/currentwallpaper.png &

    # Terminate already running mako instances
    killall -q mako
    # Wait until the processes have been shut down
    while pgrep -u $UID -x mako >/dev/null; do sleep 1; done
    # Launch mako
    mako & disown
    
    # Sleep 1 second, allowing wpgtk to properly generate icons and so forth. Race condition, might be much more reliable to find out how to wait for the wpgtk process to finish via the wait command or such, as this current approach may vary depending on computer speed. If icons are not properly generated, they must be reinstalled and then this command tweaked.
    sleep 1
    
    pkill -SIGUSR2 '^waybar$' &
done
