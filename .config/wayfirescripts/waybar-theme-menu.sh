#!/bin/bash

SELECTED=`echo -e "Make Custom Colourscheme based off of Wallpaper (Dark)
Make Custom Colourscheme based off of Wallpaper (Light)
Use Premade Colourscheme" | rofi -dmenu`

[ -z "${SELECTED}" ] && exit

case "${SELECTED}" in

    "Make Custom Colourscheme based off of Wallpaper (Dark)")
	sed -i 's/COLOURSCHEME=.*/COLOURSCHEME="WALDARK"/' ~/.config/wayfirescripts/wayfirewallpaperstate.sh
	;;

    "Make Custom Colourscheme based off of Wallpaper (Light)")
	sed -i 's/COLOURSCHEME=.*/COLOURSCHEME="WALLIGHT"/' ~/.config/wayfirescripts/wayfirewallpaperstate.sh
	;;
    
    "Use Premade Colourscheme")
	# SELECTED=`wal --theme | sed "s/ - //" | sed 's/\x1b\[[0-9;]*m//g' | head -n -44 | tail -n +2 | rofi -dmenu`

	SELECTED=`wpg --theme | rofi -dmenu`
	
	[ -z "${SELECTED}" ] && exit

	# if [ "${SELECTED}" != "Dark Themes:" ] && [ "${SELECTED}" != "Light Themes:" ]  
	# then
	#     sed -i 's/COLOURSCHEME=.*/COLOURSCHEME="'"$SELECTED"'"/' ~/.config/wayfirescripts/wayfirewallpaperstate.sh
	# fi

	sed -i 's/COLOURSCHEME=.*/COLOURSCHEME="'"$SELECTED"'"/' ~/.config/wayfirescripts/wayfirewallpaperstate.sh
	
	;;

esac

~/.config/wayfirescripts/wayfirethemerefresh.sh 
