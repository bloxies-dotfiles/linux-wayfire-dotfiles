#!/bin/sh

# Setup QT theme
mkdir ~/.config/qt5ct/colors
ln -sf ~/.cache/wal/QT5CTPywalDark.conf  ~/.config/qt5ct/colors/QT5CTPywalDark.conf 

# Setup rofi theme
mkdir ~/.config/rofi
ln -sf ~/.cache/wal/colors-rofi-dark.rasi ~/.config/rofi/config.rasi

# Setup mako theme
mkdir ~/.config/mako
ln -sf ~/.cache/wal/MakoPywal.conf ~/.config/mako/config

# Setup GTK/Icon theme
wpg-install.sh -gi
